from translate import Translator
import nltk

x = ["Arts", "Health", "News", "Science", "Society",
             "Business", "Games", "Home", "Recreation", "Sports"]

fr= Translator(to_lang="fr")
ar= Translator(to_lang="ar")
for i in x:
	print('Category '+str(i))

	f1=open('en4/'+str(i),'r')
	f2=open('fr4/'+str(i),'w')
	f3=open('ar4/'+str(i),'w')
	r=f1.read().split('#^$')
	ind=0
	for b in r:
		if ind%2==1:
			f2.write(str(fr.translate(str(b)))+'#^$')
			f3.write(str(ar.translate(str(b)))+'#^$')
		else:
			f2.write(str(b)+'#^$')
			f3.write(str(b)+'#^$')
	f1.close()
	f2.close()
	f3.close()