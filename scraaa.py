url='https://dmoztools.net/'
from bs4 import BeautifulSoup
import urllib3
from newspaper import Article
import requests
from urllib.parse import urljoin
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from bs4.element import Comment
import urllib.request
import threading
from threading import Thread
import regex as re
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

regex = re.compile(
        r'^(?:http|ftp)s?://' # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
        r'localhost|' #localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
total=0

def getLinks(page,f,browser,n):
    print('Page : '+str(page))
    browser.get(page)
    html=browser.page_source

    if n<3:
        soup=BeautifulSoup(html,'html.parser')
        for div in soup.find_all('section',class_='see-also'):
        	div.decompose()
        for div in soup.find_all('section',class_='alt-language'):
        	div.decompose()
        for div in soup.find_all('i',class_='fa-share'):
        	div.parent.parent.decompose()
        links=soup.find_all('div',class_='cat-item')
        links2=[]
        hrefs=[]
        for j in links:
        	x=j.find('a')
        	if x!=None:
                	links2.append(x)
        for r in links2:
        	hrefs.append(r.get('href'))
        sites=soup.find_all('div',class_='title-and-desc')
        sites2=[]
        for k in sites:
            sites2.append(k.find('a'))
        for l in sites2:
            if re.match(regex, l.get('href')) is not None:
                try:
                    f.write(str(page)+'#^$'+str(scrap(l.get('href')))+'#^$')
                except:
                    pass
        for i in links2:
                try:
        	        getLinks(urljoin(page,i.get('href')),f,browser,n+1)
                except:
                        print('Error. \n')


def getContent(page):
    page=requests.get(page)
    if page.status_code==200:
    	soup=BeautifulSoup(page.content,'html.parser')
    return soup.get_text()


def scrap(link):
    global total
    total=total+1
    print('total :'+str(total)+'page : '+str(link))
    article=Article(link,language='en')
    article.download()
    article.parse()
    return article.text



def tag_visible(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
    	return False
    if isinstance(element, Comment):
        return False
    return True


def text_from_html(body):
    global web
    web.get(str(body))
    html=web.page_source
    soup = BeautifulSoup(html, 'html.parser')
    texts = soup.findAll(text=True)
    visible_texts = filter(tag_visible, texts)  
    return u" ".join(t.strip() for t in visible_texts)



options = Options()
browsers={}
firefox_capabilities = DesiredCapabilities.FIREFOX
firefox_capabilities['marionette'] = True
options.add_argument("--headless")
web=webdriver.Firefox(firefox_options=options,capabilities=firefox_capabilities)
categories=['https://dmoztools.net/Arts/','https://dmoztools.net/Business/','https://dmoztools.net/Computers/','https://dmoztools.net/Games/','https://dmoztools.net/Health/','https://dmoztools.net/Home/',
'https://dmoztools.net/News/','https://dmoztools.net/Recreation/','https://dmoztools.net/Reference/','https://dmoztools.net/Regional/','https://dmoztools.net/Science/','https://dmoztools.net/Shopping/',
'https://dmoztools.net/Society/','https://dmoztools.net/Sports/','https://dmoztools.net/Kids_and_Teens/']
cat=['Family','Kids_and_Teens','Shopping','Business','Formation','Computers','Sports','Health','Science','Arts','Society','References']
nbr=0

for i in categories:
        nbr=nbr+1
        f=open('en4/'+str(nbr),'w')
        getLinks(str(i),f,web,0)
        f.close()
